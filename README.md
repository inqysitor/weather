![alt text](https://www.dropbox.com/s/3moj6us5xvwnn78/2018-03-06%2000_27_12-Weather.png?dl=1 "Weather")

# Overview
*Weather* is a sleek desktop Weather app written in Java and ReactFX.   
Build the project using Maven:  
```
mvn clean compile
mvn exec:java -Dexec.mainClass="mjunik.weather.App"
```