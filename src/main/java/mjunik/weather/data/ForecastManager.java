package mjunik.weather.data;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import javafx.animation.FadeTransition;
import javafx.scene.chart.AreaChart;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;
import mjunik.weather.control.ForecastBox;
import mjunik.weather.control.ForecastPlotCreator;
import mjunik.weather.event.ErrorEvent;
import mjunik.weather.event.EventStream;
import mjunik.weather.event.ForecastChangeEvent;
import rx.Observable;
import rx.observables.JavaFxObservable;
import rx.schedulers.JavaFxScheduler;

public class ForecastManager {

    private Map<LocalDate, Forecast> forecasts;
    private Map<LocalDate, ForecastBox> forecastBoxes; 
    
    private Pane parent;
    private StackPane hourlyChartContainer;
    private AreaChart<Number, Number> current;
    
    public ForecastManager(BorderPane parent, StackPane hourly, Observable<ForecastChangeEvent> f) {
        this.forecasts = new HashMap<LocalDate, Forecast>();
        this.forecastBoxes = new HashMap<LocalDate, ForecastBox>();
        this.hourlyChartContainer = hourly;
        
        ScrollPane scroller = new ScrollPane();
        scroller.getStyleClass().add("forecastList");
        scroller.setHbarPolicy(ScrollBarPolicy.ALWAYS);
        scroller.setVbarPolicy(ScrollBarPolicy.NEVER);
        scroller.maxWidthProperty().set(1024);
        scroller.setFitToWidth(true);
        
        HBox hbox = new HBox();
        hbox.prefHeightProperty().bind(scroller.heightProperty().subtract(10));
        hbox.getStyleClass().add("forecastListInterior");
        hbox.getStyleClass().add("spaced");
        scroller.contentProperty().set(hbox); 
        
        parent.leftProperty().set(scroller);
        this.parent = hbox;
        this.setSource(f);
    }
    
    private void setSource(Observable<ForecastChangeEvent> source){
        source.observeOn(JavaFxScheduler.getInstance()).subscribe(this::handleEvent);
    }

    private void handleEvent(ForecastChangeEvent e) {
        if(!forecasts.containsKey(e.getDate())){
            Forecast f = new Forecast(e.getDate());     
            f.setDataSource(Observable.just(e));
            forecasts.put(e.getDate(), f);
            
            ForecastBox fBox = new ForecastBox();
            fBox.setSource(f);
            forecastBoxes.put(e.getDate(), fBox);
            
            parent.getChildren().add(fBox);
            
            JavaFxObservable.eventsOf(fBox, MouseEvent.MOUSE_CLICKED)
            .subscribe(ev -> {   
                ForecastPlotCreator creator = new ForecastPlotCreator(f.getWeatherProperty());
                
                hourlyChartContainer.getChildren().clear();
                try {
                    current = creator.createPlot();
                    current.setOpacity(0.0);
                    
                    FadeTransition fadeIn = new FadeTransition(Duration.seconds(0.5), current);
                    fadeIn.setFromValue(0.0);
                    fadeIn.setToValue(1.0);
                    hourlyChartContainer.getChildren().add(current);
                    fadeIn.play();
                } catch( Exception ex ) {
                    EventStream.joinStream(Observable.just(new ErrorEvent(ex)));
                }
                
            });
        } else {
            forecasts.get(e.getDate()).setDataSource(Observable.just(e));
        }
    }
}
