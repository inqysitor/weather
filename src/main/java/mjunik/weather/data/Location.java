package mjunik.weather.data;

public class Location {

    private String location;
    private String shortLocation;
    private String countryCode;
    private String latitude;
    private String longitude;
    
    private String owmId;
    
    public String getLocation() {
        return location;
    }

    public String getShortLocation() {
        return shortLocation;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getOwmId() {
        return owmId;
    }

    public void setOwmId(String owmId) {
        this.owmId = owmId;
    }

    public Location(String location, String shortLocation, String countryCode, String latitude, String longitude) {
        this.location = location;
        this.shortLocation = shortLocation;
        this.countryCode = countryCode;
        this.latitude = latitude;
        this.longitude = longitude;
    }
    
    @Override
    public String toString() {
        return location;
    }
    
    public String getId() {
        if(latitude != null && latitude != "" && longitude != null && longitude != "") {
            return shortLocation+"-"+latitude+"-"+longitude;
        } else if (owmId != null && owmId != "") {
            return shortLocation+"-"+owmId;
        } else if (countryCode != null && countryCode != "") { 
            return shortLocation+"-"+countryCode;
        } else {
            return location;
        }
    }
}
