package mjunik.weather.data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.ListIterator;

import javafx.beans.property.FloatProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import mjunik.weather.event.ForecastChangeEvent;
import rx.Observable;
import rx.observables.JavaFxObservable;

/*
 * Class assumes it receives data with timestamps equal to ones set previously or ones that are later than the most late one already received.
 * 07:00 ok
 * 10:00 ok (later than 07:00)
 * 07:00 ok (already received 07:00)
 * 12:00 ok (later than 10:00)
 * 08:00 wrong (earlier than 12:00 and not received 08:00 already)
 */
public class Forecast {

    /*
     * I wanted this to be a LinkedHashMapProperty<LocalDateTime, WeatherState> but JavaFX doesn't have that
     * (MapProperty and all derivations of it are backed by a HashMap apparently)
     */
    private ObservableList<WeatherState> weatherProperty;
    private ObjectProperty<LocalDate> dateProperty;
    private IntegerProperty minTempProperty;
    private IntegerProperty maxTempProperty;
    private IntegerProperty pressureProperty;
    private IntegerProperty humidityProperty;
    private FloatProperty windSpeedProperty;
    private IntegerProperty windDirectionProperty;
    private IntegerProperty cloudsProperty;
    
    public Forecast(LocalDate date) {
        this.dateProperty = new SimpleObjectProperty<LocalDate>(date);
        this.weatherProperty = FXCollections.observableArrayList();
        this.minTempProperty = new SimpleIntegerProperty(100);
        this.maxTempProperty = new SimpleIntegerProperty(-274);
        this.pressureProperty = new SimpleIntegerProperty(-1);
        this.humidityProperty = new SimpleIntegerProperty(-1);
        this.windSpeedProperty = new SimpleFloatProperty(-1.0f);
        this.windDirectionProperty = new SimpleIntegerProperty(-1);
        this.cloudsProperty = new SimpleIntegerProperty(-1);
        this.createBindings();
    }
    
    public ObjectProperty<LocalDate> getDateProperty() {
        return dateProperty;
    }

    public ObservableList<WeatherState> getWeatherProperty() {
        return weatherProperty;
    }

    public IntegerProperty getMinTempProperty() {
        return minTempProperty;
    }

    public IntegerProperty getMaxTempProperty() {
        return maxTempProperty;
    }

    public IntegerProperty getPressureProperty() {
        return pressureProperty;
    }

    public IntegerProperty getHumidityProperty() {
        return humidityProperty;
    }

    public FloatProperty getWindSpeedProperty() {
        return windSpeedProperty;
    }

    public IntegerProperty getWindDirectionProperty() {
        return windDirectionProperty;
    }
    
    public IntegerProperty getCloudsProperty() {
        return cloudsProperty;
    }

    private void createBindings() {
        JavaFxObservable.additionsOf(weatherProperty).subscribe(state -> {
            if(state.isTempSet()) {
                int temp = state.getTemp();
                if(temp < minTempProperty.get()) {
                    minTempProperty.set(temp);
                }
                
                if(temp > maxTempProperty.get()) {
                    maxTempProperty.set(temp);
                }
            }
            
            if(state.isWindSpeedSet()) {
                if(state.getWindSpeed() > windSpeedProperty.get()){
                    this.windSpeedProperty.set(state.getWindSpeed());
                    if(state.isWindDirectionSet()) {
                        this.windDirectionProperty.set(state.getWindDirection());
                    }
                }
            }
            
            if(state.isHumiditySet()) {
                if(this.humidityProperty.get() == -1) {
                    this.humidityProperty.set(state.getHumidity());
                } else {
                    this.humidityProperty.set((this.humidityProperty.get()*(weatherProperty.size()-1) + state.getHumidity())/weatherProperty.size());
                }
            }
            
            if(state.isPressureSet()) {
                if(this.pressureProperty.get() == -1) {
                    this.pressureProperty.set(state.getPressure());
                } else {
                    this.pressureProperty.set((this.pressureProperty.get()*(weatherProperty.size()-1) + state.getPressure())/weatherProperty.size());
                }
            }
            
            if(state.isCloudsSet()) {
                if(this.cloudsProperty.get() == -1) {
                    this.cloudsProperty.set(state.getClouds());
                } else {
                    this.cloudsProperty.set((this.cloudsProperty.get()*(weatherProperty.size()-1) + state.getClouds())/weatherProperty.size());
                }
            }
            
        });
    }
    
    public void setDataSource(Observable<ForecastChangeEvent> e) {
        e.map(event -> event.getEntries())
        .map(unsortedList -> {
            List<WeatherState> sortedList = new ArrayList<WeatherState>(unsortedList);
            Collections.sort(sortedList, new Comparator<WeatherState>(){
                @Override
                public int compare(WeatherState o1, WeatherState o2) {
                    if(!o1.isDateSet()) {
                        return o2.isDateSet() ?  -1 : 0;
                    } else if (!o2.isDateSet()) {
                        return o1.isDateSet() ? 1 : 0;
                    }
                    
                    LocalDateTime date1 = o1.getDate();
                    LocalDateTime date2 = o2.getDate();
                    if(date1.isAfter(date2)) {
                        return 1;
                    } else if(date2.isAfter(date1)) {
                        return -1;
                    } else {
                        return 0;
                    }
                }
            });
            return sortedList;
        })
        .flatMap(list -> Observable.from(list))
        .filter(state -> state.getDate().toLocalDate().equals(dateProperty.get()))
        .subscribe(this::updateFromWeatherState);
    }  
    
    public void updateFromWeatherState(WeatherState state) {
        ListIterator<WeatherState> it = weatherProperty.listIterator();
        boolean set = false;
        while(it.hasNext() && !set) {
            WeatherState listState = it.next();
            if(state.getDate().isEqual(listState.getDate())) {
                it.remove();
                it.add(listState);
                set = true;
            }
        }
        
        if(!set) {
            weatherProperty.add(state);
        }
    }
}
