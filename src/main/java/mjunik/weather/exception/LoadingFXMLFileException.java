package mjunik.weather.exception;

@SuppressWarnings("serial")
public class LoadingFXMLFileException extends RuntimeException {

    private String filePath;
    
    public LoadingFXMLFileException(String filePath) {
        this.filePath = filePath;
    }
    
    public String toString() {
        return "Could not load FXML file from: "+filePath;
    }
}
