package mjunik.weather.network;

import java.util.List;

import com.google.gson.JsonElement;

import mjunik.weather.event.Event;

public interface JsonWebSourceData extends WebSourceData {
    public abstract String createURL();
    
    public abstract List<Event> convertToEvents(JsonElement jsonElement);
}
