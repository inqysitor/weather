package mjunik.weather.network;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import io.netty.handler.codec.http.QueryStringEncoder;
import mjunik.weather.data.Location;
import mjunik.weather.event.AutocompleteEvent;
import mjunik.weather.event.Event;

public class JsonWebSourceDataLocationLookup implements JsonWebSourceData {

    private static final String BASE_URL = "http://autocomplete.wunderground.com/aq";
    
    private String str;
    
    public JsonWebSourceDataLocationLookup(String str) {
        this.str = str;
    }
    
    @Override
    public String createURL() {
        QueryStringEncoder q = new QueryStringEncoder(BASE_URL);
        q.addParam("query", str);
        return q.toString();
    }

    @Override
    public List<Event> convertToEvents(JsonElement jsonElement) {
        AutocompleteEvent event = new AutocompleteEvent();
        JsonObject obj = jsonElement.getAsJsonObject();
        if(obj.has("RESULTS")) {
            JsonArray results = obj.get("RESULTS").getAsJsonArray();
            for(int i = 0; i < results.size(); i++) {
                JsonObject element = results.get(i).getAsJsonObject();
                String name = element.get("name").getAsString();
                String shortName = name.split(",")[0];
                String country = element.get("c").getAsString();
                String latitude = element.get("lat").getAsString();
                String longitude = element.get("lon").getAsString();
                
                Location l = new Location(name, shortName, country, latitude, longitude);
                event.addLocation(l);
            }
            
        }
        // TODO Auto-generated method stub
        List<Event> eventList = new ArrayList<Event>();
        eventList.add(event);
        return eventList;
    }

}
