package mjunik.weather.network;

import java.util.List;

import org.jsoup.nodes.Document;

import mjunik.weather.event.Event;

public interface HtmlWebSourceData extends WebSourceData {
    public abstract String createURL();
    
    public abstract List<Event> convertToEvents(Document htmlDoc);
}
