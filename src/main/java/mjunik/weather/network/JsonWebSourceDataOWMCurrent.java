package mjunik.weather.network;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import mjunik.weather.data.Location;
import mjunik.weather.data.WeatherState;
import mjunik.weather.event.Event;
import mjunik.weather.event.WeatherChangeEvent;

public class JsonWebSourceDataOWMCurrent implements JsonWebSourceData {
    
    public enum QueryType {
        ID, NAME, LATLON
    }
    
    private static final String API_KEY = "ba6fa49401aae42340fb5892773a6b15";
    private static final String BASE_URL = "http://api.openweathermap.org/data/2.5/weather";
    
    private Location location;
    private QueryType queryType;
    
    public Location getLocation() {
        return location;
    }
    
    public JsonWebSourceDataOWMCurrent(Location location, QueryType type) {
        this.location = location;
        this.queryType = type;
    }

    
    @Override
    public String createURL() {
        StringBuilder s = new StringBuilder(BASE_URL);
        
        if(queryType == QueryType.ID) {
            s.append("?id=");
            s.append(location.getOwmId());
        } else if(queryType == QueryType.NAME) {
            s.append("?q=");
            s.append(location.getShortLocation());
        } else if(queryType == QueryType.LATLON) {
            s.append("?lat=");
            s.append(location.getLatitude());
            s.append("&lon=");
            s.append(location.getLongitude());
        }
        
        s.append("&APPID=");
        s.append(API_KEY);
        return s.toString();
    }

    @Override
    public List<Event> convertToEvents(JsonElement jsonElement) {
        JsonObject jsonObject = jsonElement.getAsJsonObject();
        List<Event> eventList = new ArrayList<>();
        WeatherState state = new WeatherState();
        
        if(jsonObject.has("name")) {
            state.setLocation(jsonObject.get("name").getAsString());
        }
        
        if(jsonObject.has("main")) {
            JsonObject main = jsonObject.get("main").getAsJsonObject();
            
            if(main.has("temp")) {
                state.setTemp((int)(main.get("temp").getAsFloat()-273.15f));
            }
            
            if(main.has("pressure")) {
                state.setPressure((int)(main.get("pressure").getAsFloat()));
            }
            
            if(main.has("humidity")) {
                state.setHumidity(main.get("humidity").getAsInt());
            }
        }
        
        if(jsonObject.has("wind")) {
            JsonObject wind = jsonObject.get("wind").getAsJsonObject();
            if(wind.has("speed")) {
                state.setWindSpeed(wind.get("speed").getAsFloat());
            }
            
            if(wind.has("deg")) {
                state.setWindDirection((int)(wind.get("deg").getAsFloat()));
            }
        }
        
        if(jsonObject.has("sys")) {
            JsonObject sys = jsonObject.get("sys").getAsJsonObject();
            
            if(sys.has("sunrise")) {
                state.setSunrise(LocalDateTime.ofInstant(Instant.ofEpochMilli(sys.get("sunrise").getAsLong()*1000), ZoneId.systemDefault()));
            }
            
            if(sys.has("sunset")) {
                state.setSunset(LocalDateTime.ofInstant(Instant.ofEpochMilli(sys.get("sunset").getAsLong()*1000), ZoneId.systemDefault()));
            }
        }
        
        if(jsonObject.has("clouds")) {
            JsonObject clouds = jsonObject.get("clouds").getAsJsonObject();
            
            if(clouds.has("all")) {
                state.setClouds(clouds.get("all").getAsInt());
            }
        }
        
        if(jsonObject.has("weather")) {
            JsonObject weather = jsonObject.get("weather").getAsJsonArray().get(0).getAsJsonObject();
            if(weather.has("main")) {
                state.setWeather(weather.get("main").getAsString());
            }
            
            if(weather.has("description")) {
                state.setDesc(weather.get("description").getAsString());
            }
        }
        
        state.setTimestamp(LocalDateTime.now());
        
        WeatherChangeEvent event = new WeatherChangeEvent(location, state, this.getClass());
        eventList.add(event);
        return eventList;
    }

}
