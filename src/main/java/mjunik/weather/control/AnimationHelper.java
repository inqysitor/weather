package mjunik.weather.control;

import javafx.animation.FadeTransition;
import javafx.animation.PauseTransition;
import javafx.animation.SequentialTransition;
import javafx.beans.property.StringProperty;
import javafx.scene.Node;
import javafx.util.Duration;

public class AnimationHelper {

    public static void simpleTextFade(String newText, Node target, StringProperty property) {
        FadeTransition fadeOut = new FadeTransition(Duration.seconds(0.5), target);
        fadeOut.setFromValue(1.0);
        fadeOut.setToValue(0.0);
        fadeOut.setOnFinished(event -> property.set(newText));
        
        FadeTransition fadeIn = new FadeTransition(Duration.seconds(0.5), target);
        fadeIn.setFromValue(0.0);
        fadeIn.setToValue(1.0);

        PauseTransition pt = new PauseTransition(Duration.millis(10));
        SequentialTransition seq = new SequentialTransition(target, fadeOut, pt, fadeIn);
        seq.play();
    }
}
