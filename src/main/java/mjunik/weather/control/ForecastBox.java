package mjunik.weather.control;

import static mjunik.weather.control.AnimationHelper.simpleTextFade;
import static mjunik.weather.control.IconPicker.getIconId;

import java.io.IOException;
import java.net.URL;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

import org.kordamp.ikonli.javafx.FontIcon;

import javafx.animation.FadeTransition;
import javafx.animation.RotateTransition;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.util.Duration;
import mjunik.weather.data.Forecast;
import mjunik.weather.exception.LoadingFXMLFileException;
import rx.Observable;
import rx.observables.JavaFxObservable;

public class ForecastBox extends VBox implements Initializable {

    private static final String FXML_TEMPLATE = "/fxml/ForecastBox.fxml";
    @FXML
    private FontIcon displayIcon;
    
    @FXML
    private Text displayDate;
    
    @FXML
    private Text displayMaxTemp;
    
    @FXML
    private Text displayMinTemp;
    
    @FXML
    private Text displayPressure;
    
    @FXML 
    private Text displayHumidity;
    
    @FXML
    private Text displayWind;
    
    @FXML
    private FontIcon displayWindIcon;
    
    @FXML
    private Text displayClouds;
    
    @FXML 
    private Text txt;
    
    @FXML
    private HBox smallHourlyContainer;
    
    public ForecastBox () {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(FXML_TEMPLATE));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        this.getStyleClass().add(".forecastElem");
        try {
            fxmlLoader.load();            
        } catch (IOException exception) {
            LoadingFXMLFileException e = new LoadingFXMLFileException(FXML_TEMPLATE);
            throw e;
        }
        
        this.opacityProperty().set(0);
     }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        FadeTransition fadeIn = new FadeTransition(Duration.seconds(0.5), this);
        fadeIn.setFromValue(0.0);
        fadeIn.setToValue(1.0);
        fadeIn.play();
    }
 
    public void setSource(Forecast forecast) {
        this.displayDate.textProperty().set(forecast.getDateProperty().get().format(DateTimeFormatter.ofPattern("dd MMM")));
        JavaFxObservable.valuesOf(forecast.getMaxTempProperty())
        .subscribe( s -> simpleTextFade(String.valueOf(s)+"°C", displayMaxTemp, displayMaxTemp.textProperty()));
        
        JavaFxObservable.valuesOf(forecast.getMinTempProperty())
        .subscribe( s -> simpleTextFade("("+String.valueOf(s)+" °C)", displayMinTemp, displayMinTemp.textProperty()));
        
        JavaFxObservable.valuesOf(forecast.getPressureProperty())
        .subscribe( s -> simpleTextFade(String.valueOf(s), displayPressure, displayPressure.textProperty()));
        
        JavaFxObservable.valuesOf(forecast.getHumidityProperty())
        .subscribe( s -> simpleTextFade(String.valueOf(s), displayHumidity, displayHumidity.textProperty()));
        
        JavaFxObservable.valuesOf(forecast.getWindSpeedProperty())
        .subscribe( s -> simpleTextFade(String.valueOf(s), displayWind, displayWind.textProperty()));
        
        JavaFxObservable.valuesOf(forecast.getWindDirectionProperty())
        .map(n -> n.intValue())
        .subscribe( i -> {
            i = (i + 180)%360;
            int oldRotate = (int)displayWindIcon.getRotate();
            RotateTransition rt = new RotateTransition(Duration.millis(1000), displayWindIcon);
            rt.setByAngle((i-oldRotate)%360);
            rt.play();  
        });
        
        JavaFxObservable.valuesOf(forecast.getCloudsProperty())
        .subscribe(s -> simpleTextFade(String.valueOf(s), displayClouds, displayClouds.textProperty()));
        
        JavaFxObservable.emitOnChanged(forecast.getWeatherProperty())
        .map(ignore -> {smallHourlyContainer.getChildren().clear(); return ignore;})
        .flatMap(list -> Observable.from(list))
        .subscribe(state -> {
            VBox box = new VBox();
            box.setAlignment(Pos.CENTER);
            box.setOpacity(0.0);
            
            FontIcon icon = new FontIcon();
            icon.setIconSize(20);
            icon.setFill(Color.WHITE);
            if(state.isDescSet()) {
                if(state.isDateSet() && state.isSunriseSet() && state.isSunsetSet()) {
                    icon.setIconLiteral(getIconId(state.getDate(), state.getSunrise(), state.getSunset(), state.getDesc()));
                } else {
                    icon.setIconLiteral(getIconId(state.getDesc()));
                }
            }  
     
            Text text = new Text();
            text.getStyleClass().add("text_unimportant");
            text.textProperty().set(String.valueOf(state.getTemp()));
            
            box.getChildren().addAll(icon, text);
            smallHourlyContainer.getChildren().add(box);
            
            FadeTransition fadeIn = new FadeTransition(Duration.seconds(0.5), box);
            fadeIn.setFromValue(0.0);
            fadeIn.setToValue(1.0);
            fadeIn.play();
        });
    }

}
