package mjunik.weather.control;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import mjunik.weather.data.Location;
import mjunik.weather.event.EventStream;
import mjunik.weather.event.SourceSwitchRequestEvent;
import mjunik.weather.exception.LoadingFXMLFileException;
import rx.observables.JavaFxObservable;

public class LocationBox extends HBox {

    private static final String FXML_TEMPLATE = "/fxml/LocationBox.fxml";
    
    @FXML
    private Text locationLabel;
    
    public LocationBox(boolean first, Location location) {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(FXML_TEMPLATE));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);   
        try {
            fxmlLoader.load();            
        } catch (IOException exception) {
            LoadingFXMLFileException e = new LoadingFXMLFileException(FXML_TEMPLATE);
            throw e;
        }
       
        if(first) {
            this.getStyleClass().add("locationBoxFirst");
        } else {
            this.getStyleClass().add("locationBox");
        }
        
        this.locationLabel.textProperty().set(location.getShortLocation());
        EventStream.joinStream(
            JavaFxObservable.eventsOf(this, MouseEvent.MOUSE_CLICKED)
            .map(event -> {
                return new SourceSwitchRequestEvent(location);
            })
        );
    }
}
