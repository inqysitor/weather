package mjunik.weather.control;

import static mjunik.weather.control.AnimationHelper.simpleTextFade;
import static mjunik.weather.control.IconPicker.getIconId;

import java.io.IOException;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.kordamp.ikonli.javafx.FontIcon;

import javafx.animation.FadeTransition;
import javafx.animation.RotateTransition;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.util.Duration;
import mjunik.weather.data.Weather;
import mjunik.weather.event.PollutionChangeEvent;
import mjunik.weather.exception.LoadingFXMLFileException;
import rx.Observable;
import rx.observables.JavaFxObservable;

public class CurrentWeatherBox extends GridPane {
    
    private static final String FXML_TEMPLATE = "/fxml/CurrentWeatherBox.fxml";

    @FXML
    private FontIcon weatherIcon;
    
    @FXML
    private Text displayWeather;
    
    @FXML
    private Text displayDesc;
    
    @FXML
    private Text displayTemp;
    
    @FXML
    private Text displaySunrise;
    
    @FXML
    private Text displaySunset;
    
    @FXML
    private Text displayTimestamp;
    
    @FXML
    private Text displayPressure;
    
    @FXML
    private Text displayHumidity;
    
    @FXML
    private Text displayWind;
    
    @FXML
    private Text displayClouds;
    
    @FXML
    private Text displayPM25;
    
    @FXML
    private Text displayPM10;
    
    @FXML
    private FontIcon displayWindIcon;
    
    public CurrentWeatherBox () {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(FXML_TEMPLATE));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        
        try {
            fxmlLoader.load();            
        } catch (IOException exception) {
            LoadingFXMLFileException e = new LoadingFXMLFileException(FXML_TEMPLATE);
            throw e;
        }
        
     }
    
    public void setWeatherObj(Weather weather) {
        
        JavaFxObservable.valuesOf(weather.getSunriseProperty())
        .filter(property -> weather.isSunriseSet())
        .map(date -> date.format(DateTimeFormatter.ofPattern("HH:mm")))
        .subscribe(s -> simpleTextFade(s, displaySunrise,  displaySunrise.textProperty()));
        
        JavaFxObservable.valuesOf(weather.getSunsetProperty())
        .filter(property -> weather.isSunsetSet())
        .map(date -> date.format(DateTimeFormatter.ofPattern("HH:mm")))
        .subscribe(s -> simpleTextFade(s, displaySunset, displaySunset.textProperty()));
        

        JavaFxObservable.valuesOf(weather.getTimestampProperty())
        .filter(property -> weather.isTimestampSet())
        .map(date -> "Last updated: "+date.format(DateTimeFormatter.ofPattern("HH:mm")))
        .subscribe(s -> simpleTextFade(s, displayTimestamp, displayTimestamp.textProperty()));
        
        JavaFxObservable.valuesOf(weather.getWeatherProperty())
        .filter(property -> weather.isWeatherSet())
        .subscribe(s -> simpleTextFade(s, displayWeather, displayWeather.textProperty()));
        
        JavaFxObservable.valuesOf(weather.getDescProperty())
        .filter(property -> weather.isDescSet())
        .subscribe(s -> {
            simpleTextFade("("+s+")", displayDesc, displayDesc.textProperty());
            String iconId;
            if(weather.isSunriseSet() && weather.isSunsetSet()) {
                iconId = getIconId(LocalDateTime.now(), weather.getSunriseProperty().get(), weather.getSunsetProperty().get(), s);
            } else {
                iconId = getIconId(s);
            }
            
            weatherIcon.setIconLiteral(iconId);
            weatherIcon.setIconSize(60);
        });
        
        JavaFxObservable.valuesOf(weather.getTempProperty())
        .filter(property -> weather.isTempSet())
        .subscribe(s -> simpleTextFade(String.valueOf(s), displayTemp, displayTemp.textProperty()));
        
        JavaFxObservable.valuesOf(weather.getHumidityProperty())
        .filter(property -> weather.isHumiditySet())
        .map(d -> String.valueOf(d)+"%")
        .subscribe(s -> simpleTextFade(s, displayHumidity, displayHumidity.textProperty()));
        
        JavaFxObservable.valuesOf(weather.getPressureProperty())
        .filter(property -> weather.isPressureSet())
        .map(n -> String.valueOf(n)+" hPa")
        .subscribe(s -> simpleTextFade(s, displayPressure, displayPressure.textProperty()));
        
        JavaFxObservable.valuesOf(weather.getWindDirectionProperty())
        .filter(property -> weather.isWindDirectionSet())
        .map(p -> p.intValue())
        .subscribe(i -> {
            i = (i + 180)%360;
            int oldRotate = (int)displayWindIcon.getRotate();
            RotateTransition rt = new RotateTransition(Duration.millis(1000), displayWindIcon);
            rt.setByAngle((i-oldRotate)%360);
            rt.play();  
        });
        
        JavaFxObservable.valuesOf(weather.getWindSpeedProperty())
        .filter(property -> weather.isWindSpeedSet())
        .subscribe(s -> simpleTextFade(s.toString()+" m/s", displayWind, displayWind.textProperty()));
        
        JavaFxObservable.valuesOf(weather.getCloudsProperty())
        .filter(property -> weather.isCloudsSet())
        .subscribe(s -> simpleTextFade(String.valueOf(s)+"%", displayClouds, displayClouds.textProperty()));

    }
    
    public void setPollutionDataSource(Observable<PollutionChangeEvent> e) {
        e.subscribe(this::parsePollutionEvent);
    }
    
    private void parsePollutionEvent(PollutionChangeEvent e) {
        DecimalFormat df = new DecimalFormat("###.#");
        if(e.getPM25() >= 0) {
            displayPM25.textProperty().set(df.format(e.getPM25()));
        }
        
        if(e.getPM10() >= 0) {
            displayPM10.textProperty().set(df.format(e.getPM10()));
        }  
    }
    
    @FXML
    public void initialize() {
        FadeTransition fadeIn = new FadeTransition(Duration.seconds(0.5), this);
        fadeIn.setFromValue(0.0);
        fadeIn.setToValue(1.0);
        fadeIn.play(); 
    }
}
