package mjunik.weather.event;

import mjunik.weather.data.Location;
import mjunik.weather.network.WebSource;

public class AddLocationRequestEvent extends Event {

    private WebSource weatherSource;
    private WebSource forecastSource;
    private Location location;
    
    public AddLocationRequestEvent(Location location, WebSource weatherSource, WebSource forecastSource) {
        this.weatherSource = weatherSource;
        this.forecastSource = forecastSource;
        this.location = location;
    }

    public WebSource getWeatherSource() {
        return weatherSource;
    }
    
    public WebSource getForecastSource() {
        return forecastSource;
    }

    public Location getLocation() {
        return location;
    }
}
