package mjunik.weather.event;

public class PollutionChangeEvent extends DataEvent {

    private double pm10;
    private double pm25;
    
    public PollutionChangeEvent(double pm25, double pm10) {
        this.pm25 = pm25;
        this.pm10 = pm10;
    }
    
    public double getPM10() {
        return pm10;
    }
    
    public double getPM25() {
        return pm25;
    }
    
    @Override
    public String toString() {
        return "pm2.5:"+pm25+", pm10:"+pm10;
    }
}
