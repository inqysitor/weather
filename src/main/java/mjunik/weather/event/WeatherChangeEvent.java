package mjunik.weather.event;

import mjunik.weather.data.Location;
import mjunik.weather.data.WeatherState;
import mjunik.weather.network.WebSourceData;

public class WeatherChangeEvent extends DataEvent {

    private WeatherState state;
    private Location location;
    private Class<? extends WebSourceData> source;
    
    public WeatherChangeEvent(Location location, WeatherState state, Class<? extends WebSourceData> source) {
        this.location = location;
        this.state = state;
        this.source = source;
    }
    
    public WeatherState getState() {
        return this.state;
    }
    
    public Class<? extends WebSourceData> getSource() {
        return source;
    }
    
    public Location getLocation() {
        return location;
    }
    
    public String toString(){
        StringBuilder s = new StringBuilder("WeatherChangeEvent {");
        s.append(" source:");
        s.append(source.getName());
        s.append(", data:{");
        s.append(this.state.toString());
        s.append("}}");
        return s.toString();
    }
}
