package mjunik.weather.event;

import mjunik.weather.data.Location;

public class SourceSwitchRequestEvent extends Event {

    private Location location;
    
    public SourceSwitchRequestEvent(Location location) {
        this.location = location;
    }


    public Location getLocation() {
        return this.location;
    }
    
    @Override
    public String toString() {
        return "SourceSwitchRequestEvent("+location+")";
    }
}
