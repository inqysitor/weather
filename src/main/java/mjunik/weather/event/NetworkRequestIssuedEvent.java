package mjunik.weather.event;

public final class NetworkRequestIssuedEvent extends Event {

	@Override
	public java.lang.String toString() {
		return "NetworkRequestIssuedEvent()";
	}

}
